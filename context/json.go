package pcs_service_context

import (
	"database/sql"
	"fmt"
	"io"

	"bitbucket.org/headmadepro/pcs_service"

	"github.com/gin-gonic/gin"
)

var jsonContentType = []string{"application/json; charset=utf-8"}

func StreamJSON(c *gin.Context, rows *sql.Rows, model pcs_service.IModel) {
	first := true
	c.Writer.Header()["Content-Type"] = jsonContentType
	c.Stream(func(w io.Writer) bool {
		if !rows.Next() && !first {
			fmt.Fprint(w, "]")
			return false
		}
		if first {
			fmt.Fprint(w, "[")
			first = false
		} else {
			fmt.Fprint(w, ",")
		}
		fmt.Fprint(w, model.Scan(rows))
		return true
	})
}
