package pcs_service

import "github.com/spf13/viper"

func initConfig() {
	viper.SetEnvPrefix("pcs")
	viper.SetDefault("env", "development")
	viper.BindEnv("env")
	viper.SetDefault("port", "9901")
	viper.BindEnv("port")
	viper.SetDefault("db", true)

	viper.SetConfigName("database")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		viper.Set("db", false)
		//panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func ReleaseMode() bool {
	return viper.GetString("env") == "production"
}
