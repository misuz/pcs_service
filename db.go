package pcs_service

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
)

func NewDbConnection() (*gorm.DB, error) {
	dbConfig := viper.GetStringMapString(viper.GetString("env"))
	connStr := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		dbConfig["host"], dbConfig["port"], dbConfig["dbname"], dbConfig["user"], dbConfig["pass"])
	db, err := gorm.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	db.DB().SetMaxIdleConns(1)
	db.DB().SetMaxOpenConns(50)
	db.DB().Ping()
	if !ReleaseMode() {
		db.LogMode(true)
	}
	return db, nil
}
