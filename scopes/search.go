package scopes

import (
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func typeOf(q string) string {
	arr := strings.Split(q, ".")
	if _, err := strconv.Atoi(arr[0]); err == nil {
		return "code"
	} else {
		arr = strings.Split(q, " ")
		if _, err := strconv.Atoi(arr[0]); err == nil {
			return "code"
		}
	}
	return "title"
}

func SearchScope(c *gin.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		query := "q"
		if value := c.Query(query); value != "" {
			switch typeOf(value) {
			case "code":
				db = db.Where("code LIKE (?)", value+"%").Order("LENGTH(code) ASC")
			case "title":
				db = db.Where("title ILIKE (?)", "%"+value+"%").Order("LENGTH(title) ASC")
			}
		}
		return db
	}
}
func AppendableSearchScope(c *gin.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		query := "q"
		if value := c.Query(query); value != "" {
			db = db.Where("line ILIKE (?)", value+"%")
		}
		return db
	}
}
