package scopes

import (
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func LimitOffsetScope(c *gin.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		limit_query := c.DefaultQuery("limit", "10")
		offset_query := c.DefaultQuery("offset", "0")
		limit, _ := strconv.Atoi(limit_query)
		offset, _ := strconv.Atoi(offset_query)
		return db.Limit(limit).Offset(offset)
	}
}

func IdsArrayScope(c *gin.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		queryIds := c.Query("ids")
		if queryIds == "" {
			return db
		}
		ids := strings.Split(queryIds, ",")
		if len(ids) > 0 {
			db = db.Where("id IN (?)", ids)
		}
		return db
	}
}

func DefaultQueryScope(c *gin.Context, query string, defaultValue string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		value := c.DefaultQuery("type", defaultValue)
		return db.Where(query+"= ?", value)
	}
}

func QueryScope(c *gin.Context, query string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if value := c.Query(query); value != "" {
			return db.Where(query+"=?", value)
		} else {
			return db
		}
	}
}

func QueriesScope(c *gin.Context, queries []string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		for _, query := range queries {
			db = db.Scopes(QueryScope(c, query))
		}
		return db
	}
}

func ChildsScope(parent string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("parent_directory_id=?", parent)
	}
}

func ParamScope(c *gin.Context, param string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if value := c.Param(param); value != "" {
			return db.Where(param+"=?", value)
		} else {
			return db
		}
	}
}

func ParamQueryStringMapScope(c *gin.Context, m map[string]string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		for p, q := range m {
			if p := c.Param(p); len(p) > 0 {
				return db.Where(q+" ILIKE ?", "%"+p+"%")
			}
			if p := c.Query(p); len(p) > 0 {
				return db.Where(q+" ILIKE ?", "%"+p+"%")
			}
		}
		return db
	}
}
