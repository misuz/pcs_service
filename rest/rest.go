package rest

import (
	"strings"

	"github.com/gin-gonic/gin"
)

// All of the methods are the same type as HandlerFunc
// if you don't want to support any methods of CRUD, then don't implement it
type CreateSupported interface {
	CreateHandler(*gin.Context)
}
type ListSupported interface {
	ListHandler(*gin.Context)
}
type TakeSupported interface {
	TakeHandler(*gin.Context)
}
type UpdateSupported interface {
	UpdateHandler(*gin.Context)
}
type DeleteSupported interface {
	DeleteHandler(*gin.Context)
}

func removeFormat(format string) gin.HandlerFunc {
	return func(c *gin.Context) {
		for i, _ := range c.Params {
			c.Params[i].Value = strings.TrimSuffix(c.Params[i].Value, "."+format)
		}
		c.Next()
	}
}

// It defines
//   POST: /path
//   GET:  /path
//   PUT:  /path/:id
//   POST: /path/:id
func CRUD(group *gin.RouterGroup, path string, format string, resource interface{}) {
	group.Use(removeFormat(format))
	index_path := path
	if path[:1] != ":" {
		index_path = path + "." + format
	}
	if resource, ok := resource.(CreateSupported); ok {
		group.POST(index_path, resource.CreateHandler)
	}
	if resource, ok := resource.(ListSupported); ok {
		group.GET(index_path, resource.ListHandler)
	}
	if resource, ok := resource.(TakeSupported); ok {
		group.GET(path+"/:id", resource.TakeHandler)
	}
	if resource, ok := resource.(UpdateSupported); ok {
		group.PUT(path+"/:id", resource.UpdateHandler)
	}
	if resource, ok := resource.(DeleteSupported); ok {
		group.DELETE(path+"/:id", resource.DeleteHandler)
	}
}
