package pcs_service

import (
	"log"

	"github.com/electrotumbao/contrib/cors"
	"github.com/gin-gonic/contrib/expvar"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
)

const DbKey = "DB"

func InitServ() *gin.Engine {
	initConfig()
	if ReleaseMode() {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.Default()
	router.GET("/debug/vars", expvar.Handler())
	corsConfig := cors.DefaultConfig()
	corsConfig.AddAllowedHeaders(
		"x-auth-token",
		"x-csrf-token",
		"Access-Control-Allow-Origin",
		"Accept",
		"Origin",
		"x-requested-with",
		"Current-Organization-Id",
	)
	router.Use(cors.New(corsConfig))
	if viper.GetBool("db") {
		db, err := NewDbConnection()
		if err != nil {
			log.Fatal(err)
		}
		router.Use(dbSet(db))
	}
	return router
}

func Run(router *gin.Engine) error {
	return router.Run(":" + viper.GetString("port"))
}

func dbSet(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set(DbKey, db)
		c.Next()
	}
}
